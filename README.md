Checkers game
===========

This is Java game made in framework supplied by School of Electrical engineering in Belgrade. It is made in Java 1.7 
and in Eclipse Kepler IDE.

To run:

-Navigate to the program in the command prompt
-Type "java -jar Checkers.jar " followed which players you want playing in the order: red_player , black_player. 
    Options for players are:
      - ui.Human (A Human Player)
      - md100040d.AlphaBeta (A player using a basic AlphaBeta algorithm)
      - md100040d.Md100040d (A player using an optimized AlphaBeta algorithm in tandem with a improved board 
      evaluator)
      
-Enjoy!