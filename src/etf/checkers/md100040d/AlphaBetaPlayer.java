package etf.checkers.md100040d;

import java.util.List;
import static etf.checkers.CheckersConsts.*;
import etf.checkers.*;

/**
 * Represents one AI player and does generic Alpha - Beta calculation. Evaluator
 * being used is pretty basic, but still it runs great on smaller time slice.
 * 
 * @author Dejan Markovic
 * 
 */
public class AlphaBetaPlayer extends CheckersPlayer implements
		GradedCheckersPlayer {

	// konstante
	public static final int MINUS_INF = Integer.MIN_VALUE;
	public static final int PLUS_INF = Integer.MAX_VALUE;

	// polja
	protected int pruneCount;// brojanje odsecenih podstabala
	protected Evaluator sbe;// koristiti za procenu terminalnog cvora
	protected BoardState trenutnoStanjeTable;// tekuce stanje na tabli u algoritmu
	protected int najboljiRezultat;// najbolji do sada pronadjeni rezultat
	protected Move najboljiPotez;// i potez koji obezbedjuje taj potez
	protected int poslednjiOdseceniCvor = MINUS_INF;// brojanje odsecenih grana po
												// iteraciji

	/**
	 * Constructor of Alpha - Beta player
	 * 
	 * @param name
	 *            Name of player that will be used in GUI representation
	 * @param side
	 *            0 or 1 depending on color of player
	 */
	public AlphaBetaPlayer(String name, int side) {
		super(name, side);
		sbe = new SimpleEvaluator();
	}

	/**
	 * Based on current state of table does calculation, by generating tree.
	 * 
	 * @param bs
	 *            array containing current state of table
	 */
	public void calculateMove(int[] bs) {

		/* Napravimo pomocnu tablu za cuvanje privremenog stanja u racunu */
		trenutnoStanjeTable = new BoardState(bs, side);

		/* Za tekuceg igraca dohvatimo sve moguce poteze. */
		List<Move> moguciPotezi = trenutnoStanjeTable.getAllPossibleMoves();

		/* Ako nema vise dozvoljenih poteza kraj racuna. */
		if (moguciPotezi.size() == 0)
			return;

		/*
		 * Samo racunamo iterativno do nivoa koji je preciziran limitom, ako
		 * nista nije receno ide se do 1000-tog nivoa. --dubinalimit num opcija
		 * ovo specificira
		 */

		int dubina = 0;
		while (dubina < depthLimit) {

			najboljiRezultat = MINUS_INF;
			najboljiPotez = null;

			/* Prolazimo kroz sve poteze koji su moguci u tekucem stanju */
			for (Move move : moguciPotezi) {

				/*
				 * Svaki moguci potez izvrsimo da bi smo na osnovu takvog stanja
				 * evaluirali iducu mogucnost
				 */
				trenutnoStanjeTable.execute(move);

				/*
				 * Alfa i Beta vrednost za Root cvor su minus INF i max INF.
				 * Metoda min sprovodi algoritam razmisljanja min igraca. Nivo
				 * je 0 za koji se poziva na pocetku i prosledjuje se trenutno
				 * stanje table za rekurzivno razmisljanje.
				 */
				int tmpScore = min(MINUS_INF, PLUS_INF, dubina);

				/* Ponistimo poslednji potez */
				trenutnoStanjeTable.revert();

				/*
				 * Ako je dobijeni rezultat bolji od prethodnog uzeti novu
				 * vrednost za vrednost cvora. Takodje zabelezimo i taj najbolji
				 * potez za koji cemo dobiti taj rezultat.
				 */
				if (najboljiRezultat < tmpScore) {
					najboljiPotez = move;
					setMove(move);
					najboljiRezultat = tmpScore;
				}

			}

			/* Promeniti */
			if (Utils.verbose == true) {
				System.out.println("Najbolji potez: " + najboljiPotez
						+ "\t\tRezultat: " + najboljiRezultat
						+ "\t\tOdsecenih podstabala: " + pruneCount);
			}

			/* Povecamo nivo za koji vrsimo razmatranje */
			dubina = dubina + 2;

		}

	}

	/**
	 * Recursive function for max player level
	 * 
	 * @param alfa_vrednost
	 *            Alpha value passed from its parent node
	 * @param beta_vrednost
	 *            Beta value passed from its parent node
	 * @param dubina
	 *            current depth of tree
	 * @return score on current node
	 */
	protected int max(int alfa_vrednost, int beta_vrednost, int dubina) {

		int val = alfa_vrednost;

		/* Dohvatimo moguce poteze za tekuce stanje na ploci. (Slicno kao gore) */
		List<Move> moguciPotezi = trenutnoStanjeTable.getAllPossibleMoves();

		/*
		 * Ako nema dostupnih poteza ili smo stigli do terminalne pozicije
		 * izracunamo stanje i vratimo ga nazad kao rezultat.
		 */
		if (moguciPotezi.size() == 0 || dubina == 0) {

			int tmpScore = sbe.evaluate(trenutnoStanjeTable.D);

			/*
			 * U zavisnosti da li je MAX ili MIN igrac vracamo pozitivan ili
			 * negativan score za tekuce stanje.
			 */
			if (side == BLK) {
				return -tmpScore;
			} else {
				return tmpScore;
			}

		} else {

			for (int i = 0; i < moguciPotezi.size(); i++) {

				/* Izvrsimo potez da bi mogli da sprovodimo algoritam dalje */
				trenutnoStanjeTable.execute(moguciPotezi.get(i));

				/*
				 * Ako na ovoj strani imamo bolji rezultat promenimo alfa
				 * vrednost
				 */
				int tmpScore = min(alfa_vrednost, beta_vrednost, dubina - 1);

				if (alfa_vrednost < tmpScore) {
					alfa_vrednost = tmpScore;
				}
				val = alfa_vrednost;

				/* Restauriramo prethodno stanje. (Rekurzivno vracanje unazad) */
				trenutnoStanjeTable.revert();

				/* AKo je beta palo ispod Alfa radimo odsecanje. */
				if (beta_vrednost <= alfa_vrednost) {
					poslednjiOdseceniCvor = alfa_vrednost;// zabelezimo vrednost na
													// cvoru
					pruneCount++;// uvecamo broj odsecanja koja smo obavili
					val = beta_vrednost;
				}
			}

			if (Utils.verbose)
				System.out.println("minimax zadnjeg odsecenog: "
						+ poslednjiOdseceniCvor);

			/* Ako nije radjeno odsecanje vracamo alfa vrednost. */
			return val;
		}
	}

	/**
	 * Recursive function for min player level
	 * 
	 * @param alfa_vrednost
	 *            Alpha value passed from its parent node
	 * @param beta_vrednost
	 *            Beta value passed from its parent node
	 * @param dubina
	 *            current depth of tree
	 * @return score on current node
	 */
	protected int min(int alfa_vrednost, int beta_vrednost, int dubina) {

		int val = beta_vrednost;

		List<Move> moguciPotezi = trenutnoStanjeTable.getAllPossibleMoves();

		/* AKo je temrinalni cvor ili smo stigli do granice dubine. */
		if (moguciPotezi.size() == 0 || dubina == 0) {
			int tmpScore = sbe.evaluate(trenutnoStanjeTable.D);
			if (side == BLK)
				return -tmpScore;
			else
				return tmpScore;
		} else {

			for (int i = 0; i < moguciPotezi.size(); i++) {

				/*
				 * Odigrati potez pa za tako izracunati potez racnuati da li se
				 * promenila beta vrednost.
				 */

				trenutnoStanjeTable.execute(moguciPotezi.get(i));

				/*
				 * Izracunati novi rezultat i ako je manji od prethodne beta
				 * vrednosti zabeleziti istu.
				 */
				int tmpScore = max(alfa_vrednost, beta_vrednost, dubina - 1);
				if (tmpScore < beta_vrednost) {
					beta_vrednost = tmpScore;
				}
				val = beta_vrednost;

				/* Vratimo na prethodno stanje */
				trenutnoStanjeTable.revert();

				/* AKo je beta manje od alfa radimo odsecanje. */
				if (beta_vrednost <= alfa_vrednost) {
					poslednjiOdseceniCvor = beta_vrednost;
					pruneCount++;
					val = alfa_vrednost;// u ovom slucaju vracamo alfa, a ako
										// nismo
					// odsecali vracamo beta vrednost
				}
			}

			if (Utils.verbose)
				System.out.println("minimax zadnjeg odsecenog: "
						+ poslednjiOdseceniCvor);

			return val;
		}
	}

	/**
	 * Returns number of pruned sub trees
	 * 
	 */
	@Override
	public int getPruneCount() {
		return pruneCount;
	}

	/**
	 * Return score of last pruned node in algorithm.
	 */
	@Override
	public int getLastPrunedNodeScore() {
		return poslednjiOdseceniCvor;
	}

}
