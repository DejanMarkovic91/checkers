package etf.checkers.md100040d;

import static etf.checkers.CheckersConsts.BLK;

import java.util.*;

import etf.checkers.*;

/**
 * Represents improved Alpha - Beta player. Uses sorting of moves to get best
 * move faster. Also it uses ImprovedEvaluator that is much better than simple
 * evaluator used in ordinary Alpha - Beta player. Heuristic function is also
 * using dynamic value of figures during time and is using different values for
 * pawns and kings on board positions, and also preventing kings from being
 * stuck on edges of board.
 * 
 * @author Dejan Markovic
 * 
 */
public class Md100040dPlayer extends AlphaBetaPlayer {

	/**
	 * Constructor for Player.
	 * 
	 * @param name
	 *            Name of player that is being used in GUI representation
	 * @param side
	 *            0 or 1 depending on color of player
	 */
	public Md100040dPlayer(String name, int side) {
		super(name, side);
		sbe = new ImprovedEvaluator();
	}

	public void calculateMove(int[] bs) {

		/* Napravimo pomocnu tablu za cuvanje privremenog stanja u racunu */
		trenutnoStanjeTable = new BoardState(bs, side);

		/* Za tekuceg igraca dohvatimo sve moguce poteze. */
		List<Move> moguciPotezi = trenutnoStanjeTable.getAllPossibleMoves();

		/* Ako nema vise dozvoljenih poteza kraj racuna. */
		if (moguciPotezi.size() == 0)
			return;

		setMove(moguciPotezi.get(0));

		/* Prioritet imaju operacije skidanja protivnikovih figura */
		/*Collections.sort(moguciPotezi, new Comparator<Move>() {

			@Override
			public int compare(Move o1, Move o2) {
				return Utils.isWalk(o2) && !Utils.isWalk(o1) ? -1 : (Utils
						.isWalk(o2) && Utils.isWalk(o1) ? 0 : 1);
			}

		});*/

		int dubina = 0;
		while (dubina < depthLimit) {

			najboljiRezultat = MINUS_INF;
			najboljiPotez = null;

			/* Prolazimo kroz sve poteze koji su moguci u tekucem stanju */
			for (Move move : moguciPotezi) {

				/*
				 * Svaki moguci potez izvrsimo da bi smo na osnovu takvog stanja
				 * evaluirali iducu mogucnost
				 */
				trenutnoStanjeTable.execute(move);

				/*
				 * Alfa i Beta vrednost za Root cvor su minus INF i max INF.
				 * Metoda min sprovodi algoritam razmisljanja min igraca. Nivo
				 * je 0 za koji se poziva na pocetku i prosledjuje se trenutno
				 * stanje table za rekurzivno razmisljanje.
				 */
				int tmpScore = min(MINUS_INF, PLUS_INF, dubina);

				/* Ponistimo poslednji potez */
				trenutnoStanjeTable.revert();

				/*
				 * Ako je dobijeni rezultat bolji od prethodnog uzeti novu
				 * vrednost za vrednost cvora. Takodje zabelezimo i taj najbolji
				 * potez za koji cemo dobiti taj rezultat.
				 */

				if (najboljiRezultat < tmpScore) {
					najboljiPotez = move;
					setMove(move);
					najboljiRezultat = tmpScore;
				}

			}

			/* Promeniti */
			if (Utils.verbose == true) {
				System.out.println("Najbolji potez: " + najboljiPotez
						+ "\t\tRezultat: " + najboljiRezultat
						+ "\t\tOdsecenih podstabala: " + pruneCount);
			}

			/* Povecamo nivo za koji vrsimo razmatranje */
			dubina = dubina + 2;

		}

	}

	protected int max(int alfa_vrednost, int beta_vrednost, int dubina) {

		int val = alfa_vrednost;

		/* Dohvatimo moguce poteze za tekuce stanje na ploci. (Slicno kao gore) */
		List<Move> moguciPotezi = trenutnoStanjeTable.getAllPossibleMoves();

		/*
		 * Ako nema dostupnih poteza ili smo stigli do terminalne pozicije
		 * izracunamo stanje i vratimo ga nazad kao rezultat.
		 */
		if (moguciPotezi.size() == 0 || dubina == 0) {

			int tmpScore = sbe.evaluate(trenutnoStanjeTable.D);

			/*
			 * U zavisnosti da li je MAX ili MIN igrac vracamo pozitivan ili
			 * negativan score za tekuce stanje.
			 */
			if (side == BLK) {
				return -tmpScore;
			} else {
				return tmpScore;
			}

		} else {

			Collections.sort(moguciPotezi, new Comparator<Move>() {

				@Override
				public int compare(Move o1, Move o2) {
					return Utils.isWalk(o2) && !Utils.isWalk(o1) ? -1 : (Utils
							.isWalk(o2) && Utils.isWalk(o1) ? 0 : 1);
				}

			});

			for (Move move : moguciPotezi) {

				/* Izvrsimo potez da bi mogli da sprovodimo algoritam dalje */
				trenutnoStanjeTable.execute(move);

				/*
				 * Ako na ovoj strani imamo bolji rezultat promenimo alfa
				 * vrednost
				 */
				int tmpScore = min(alfa_vrednost, beta_vrednost, dubina - 1);

				if (alfa_vrednost < tmpScore) {
					alfa_vrednost = tmpScore;
				}
				val = alfa_vrednost;

				/* Restauriramo prethodno stanje. (Rekurzivno vracanje unazad) */
				trenutnoStanjeTable.revert();

				/* AKo je beta palo ispod Alfa radimo odsecanje. */
				if (beta_vrednost <= alfa_vrednost) {
					poslednjiOdseceniCvor = beta_vrednost;// zabelezimo vrednost
															// na
					// cvoru
					pruneCount++;// uvecamo broj odsecanja koja smo obavili
					val = alfa_vrednost;

					if (Utils.verbose) {
						System.out
								.println("Minimax vrednost poslednjeg odsecenog cvora: "
										+ poslednjiOdseceniCvor);
					}
				}
			}

			/* Ako nije radjeno odsecanje vracamo alfa vrednost. */
			return val;
		}
	}

	protected int min(int alfa_vrednost, int beta_vrednost, int dubina) {

		int val = beta_vrednost;

		List<Move> moguciPotezi = trenutnoStanjeTable.getAllPossibleMoves();

		/* AKo je temrinalni cvor ili smo stigli do granice dubine. */
		if (moguciPotezi.size() == 0 || dubina == 0) {
			int tmpScore = sbe.evaluate(trenutnoStanjeTable.D);
			if (side == BLK)
				return -tmpScore;
			else
				return tmpScore;
		} else {

			Collections.sort(moguciPotezi, new Comparator<Move>() {

				@Override
				public int compare(Move o1, Move o2) {
					return Utils.isWalk(o2) && !Utils.isWalk(o1) ? 1 : (Utils
							.isWalk(o2) && Utils.isWalk(o1) ? 0 : -1);
				}

			});

			for (Move move : moguciPotezi) {

				/*
				 * Odigrati potez pa za tako izracunati potez racnuati da li se
				 * promenila beta vrednost.
				 */

				trenutnoStanjeTable.execute(move);

				/*
				 * Izracunati novi rezultat i ako je manji od prethodne beta
				 * vrednosti zabeleziti istu.
				 */
				int tmpScore = max(alfa_vrednost, beta_vrednost, dubina - 1);
				if (tmpScore < beta_vrednost) {
					beta_vrednost = tmpScore;
				}
				val = beta_vrednost;

				/* Vratimo na prethodno stanje */
				trenutnoStanjeTable.revert();

				/* AKo je beta manje od alfa radimo odsecanje. */
				if (beta_vrednost <= alfa_vrednost) {
					poslednjiOdseceniCvor = alfa_vrednost;
					pruneCount++;
					val = beta_vrednost;// u ovom slucaju vracamo alfa, a ako
										// nismo
					// odsecali vracamo beta vrednost

					if (Utils.verbose) {
						System.out
								.println("Minimax vrednost poslednjeg odsecenog cvora: "
										+ poslednjiOdseceniCvor);
					}
				}
			}

			return val;
		}
	}

}
