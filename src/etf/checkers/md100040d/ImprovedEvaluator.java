package etf.checkers.md100040d;

import etf.checkers.SimpleEvaluator;
import static etf.checkers.CheckersConsts.*;

/**
 * Calculates value of board depending on its current state.
 * 
 * @author Dejan Markovic
 * 
 */
public class ImprovedEvaluator extends SimpleEvaluator {
	public static final int size = H * W;

	/**
	 * Returns score of current state.
	 * 
	 * @param bs
	 *            array containing state of board
	 */
	@Override
	public int evaluate(int[] bs) {
		int valK = 0;
		int valP = 0;
		int brCrvenih = 0;
		int brCrnih = 0;
		int trenutniRezultat = 0;
		int ukupnoFigura = 0;
		int crniIvica = 0;
		int crveniIvica = 0;

		for (int i = 0; i < size; i++) {
			switch (bs[i]) {
			case RED_PAWN:
			case RED_KING:
				brCrvenih++;
				break;
			case BLK_PAWN:
			case BLK_KING:
				brCrnih++;
				break;
			}
		}

		ukupnoFigura = brCrvenih + brCrnih;

		if (ukupnoFigura > 14)
			trenutniRezultat = ((brCrvenih) - (brCrnih)) * 64;
		else if (ukupnoFigura > 8)
			trenutniRezultat = ((brCrvenih) - (brCrnih))
					* (152 - (ukupnoFigura) * 11);
		else
			trenutniRezultat = ((brCrvenih) - (brCrnih))
					* (192 - (ukupnoFigura) * 16);

		if (ukupnoFigura >= 20) {
			valP = 4;
			valK = 6;
		} else if (ukupnoFigura >= 14) {
			valP = 10;
			valK = 14;
		} else if (ukupnoFigura >= 10) {
			valP = 18;
			valK = 20;
		}

		for (int i = 0; i < size; i++) {
			switch (bs[i]) {
			case RED_PAWN:
				trenutniRezultat += (valP + pawn_value(i));
				break;
			case BLK_PAWN:
				trenutniRezultat -= (valP + pawn_value(i));
				break;
			case RED_KING:
				trenutniRezultat += (valK + king_value(i));
				if (edgePosition(i))
					crveniIvica++;
				break;
			case BLK_KING:
				trenutniRezultat -= (valK + king_value(i));
				if (edgePosition(i))
					crniIvica++;
				break;
			default:
				break;
			}

		}
		trenutniRezultat -= 4 * (crveniIvica - crniIvica);
		return trenutniRezultat;
	}

	/**
	 * Determines if figure on board is on side edge of board. (Used for kings,
	 * since that is really bad poition for them)
	 * 
	 * @param i
	 *            index in array of state of board
	 * @return is it on edge or not
	 */
	public boolean edgePosition(int i) {
		switch (i) {
		case 8:
		case 24:
		case 40:
		case 55:
		case 7:
		case 23:
		case 39:
		case 56:
			return true;
		default:
			return false;
		}
	}

	/**
	 * Determines pawn value on board depending on position of pawn in array of
	 * board state
	 * 
	 * @param i
	 *            index in array of board state
	 * @return pawn value
	 */
	protected int pawn_value(int i) {
		int val = 0;
		switch (i) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 58:
		case 60:
		case 62:
			val = 4;
			break;
		case 10:
		case 12:
		case 14:
		case 26:
		case 28:
		case 35:
		case 37:
			val = 3;
			break;
		case 17:
		case 19:
		case 21:
		case 42:
		case 44:
		case 46:
			val = 2;
			break;
		default:
			break;// 0
		}
		return val;
	}

	/**
	 * Determines king value on board depending on position of king in array of
	 * board state
	 * 
	 * @param i
	 *            index in array of board state
	 * @return king value
	 */
	protected int king_value(int i) {
		int val = 0;
		switch (i) {
		case 10:
		case 12:
		case 14:
		case 49:
		case 51:
		case 53:
			val = 1;
			break;
		case 19:
		case 21:
		case 26:
		case 30:
		case 42:
		case 44:
		case 46:
			val = 2;
			break;
		case 28:
		case 35:
		case 37:
			val = 4;
			break;
		default:
			break;// 0
		}
		return val;
	}

}
